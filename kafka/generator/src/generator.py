# Generates random customers for a website

import argparse
from faker import Faker
import geocoder
from random import choice
from random import randint

parser = argparse.ArgumentParser(__file__, description="Web Server Data Generator")
parser.add_argument("--num_customers", "-u", type=int, dest="num_customers",
                    help="The number of customers to create", default=1000)
parser.add_argument("--num_drivers", "-d", type=int, dest="num_drivers",
                    help="The number of drivers to create", default=100)
parser.add_argument("--num_bookings", "-b", type=int, dest="num_bookings",
                    help="The number of bookings to create", default=10000)

args = parser.parse_args()
num_customers = int(args.num_customers)
num_drivers = int(args.num_drivers)
num_bookings = int(args.num_bookings)


faker = Faker()
# A list of functions for generating customer agent strings for various browsers
ualist = [faker.firefox, faker.chrome, faker.safari, faker.internet_explorer, faker.opera]

def generate_customer():
    """
    Returns a randomly generate dictionary representing a customer, where each customer is described by
    a customer agent string, an ID, a latlng, an IP, an age_bracket, whether they've oped into marketing
    and the
    :return:
    """
    customer = {}
    customer['id'] = faker.random_int(1, 1000)
    customer['name'] = faker.name()
    customer['customer_agent'] = choice(ualist)()
    customer['age_bracket'] = choice(['18-25', '26-40', '41-55', '55+'])
    customer['job'] = faker.job()
    customer['phone'] = faker.phone_number()
    customer['email'] = faker.email()
    customer['opted_into_marketing'] = choice([True, False])
    return customer

def generate_driver():
    """
    Returns a randomly generate dictionary representing a customer, where each customer is described by
    a customer agent string, an ID, a latlng, an IP, an age_bracket, whether they've oped into marketing
    and the
    :return:
    """
    driver = {}
    driver['id'] = faker.random_int(1, 100)
    driver['driver_name'] = faker.name()
    driver['phone'] = faker.phone_number()
    driver['email'] = faker.email()
    driver['age_bracket'] = choice(['18-25', '26-40', '41-55', '55+'])
    driver['vehicle'] = choice(['Van', 'Truck', 'Economy', 'Motor'])
    driver['rated'] = choice([1,2,3,4,5])
    return driver

def generate_booking(customers, drivers):
    """
    Returns a randomly generate dictionary representing a customer, where each customer is described by
    a customer agent string, an ID, a latlng, an IP, an age_bracket, whether they've oped into marketing
    and the
    :return:
    """
    booking = {}
    booking['id'] = faker.random_int(1, 10000)
    booking['status'] = choice(['Locating Driver', 'Driver Accepted', 'In Progress', 'Completed', 'Canceled'])
    booking['customer_id'] = choice(customers)['id']
    booking['driver_id'] = choice(drivers)['id']
    booking['pickup_address'] = faker.address()
    booking['created_at'] = faker.past_datetime()
    return booking

def write_csvs(objects, **kwargs):
    """
    Writes two .csv files, one for ingestiong by an event generator, the other formatted to be uploaded to BigQuery
    :param customers:
    :return:
    """
    file_name = kwargs['name']
    with open(f"{file_name}.csv", 'w') as event_out:
            cols = list(objects[0].keys())
            cols.sort()
            event_out.write(",".join(cols) + '\n')
            for item in objects:
                event_vals = [str(item[key]) for key in cols]
                event_out.write(",".join(event_vals) + '\n')

if __name__ == '__main__':
    customers = [generate_customer() for i in range(num_customers)]
    write_csvs(customers, name='customers')
    drivers = [generate_driver() for i in range(num_drivers)]
    write_csvs(drivers, name='drivers')
    bookings = [generate_booking(customers, drivers) for i in range (num_bookings)]
    write_csvs(bookings, name='bookings')