from minio import Minio
from minio.error import S3Error

# https://github.com/minio/minio-py
def main():
   # Create a client with the MinIO server playground, its access key
   # and secret key.
   client = Minio(
       "minio:9000",
       access_key="YourAccessKey",
       secret_key="YourSecretKey",
       secure=False,
   )

   # Make 'demo' bucket if not exist.
   found = client.bucket_exists("demo")
   if not found:
       client.make_bucket("demo")
   else:
       print("Bucket 'demo' already exists")

   # Upload 'minio.jpg' as object name
   # 'minio.jpg' to bucket 'demo'.
   client.fput_object(
       "demo",
       "minio.jpg",
       "./minio.jpg",
   )
   print(
       "'minio.jpg' is successfully uploaded as "
       "object 'minio.jpg' to bucket 'demo'."
   )


if __name__ == "__main__":
   try:
       main()
   except S3Error as exc:
       print("error occurred.", exc)